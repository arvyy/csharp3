﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace WindowsFormsApplication1
{
    static class Program
    {
        static void Main(string[] args)
        {

            using (var m = new Model1Container())
            {
                
                Event e1 = new Event() { Id = 1, Title = "Protmusis", Description = "Protmusis Vilniaus g.", From = new DateTime(2016, 12, 13, 14, 0, 0), Till = new DateTime(2016, 12, 13, 18, 0, 0) };
                Event e2 = new Event() { Id = 2, Title = "Koncertas", Description = "Koncertas Siemens arenoje", From = new DateTime(2016, 12, 13, 18, 0, 0), Till = new DateTime(2016, 12, 13, 21, 30, 0) };
                User u1 = new User() { Id = 1, Name = "Petras"};
                User u2 = new User() { Id = 2, Name = "Jonas" };
                Comment c1 = new Comment() { Id = 1, Text = "Idomus koncertas"};
                Comment c2 = new Comment() { Id = 2, Text = "Idomus protmusis" };
                Comment c3 = new Comment() { Id = 3, Text = "Tikrai idomus protmusis" };


                e1.Users.Add(u1);
                e1.Users.Add(u2);
                c1.User = u1;
                c2.User = u2;
                c3.User = u1;
                e1.Comments.Add(c2);
                e2.Comments.Add(c1);
                m.Events.Add(e1);
                m.Events.Add(e2);
                m.Comments.Add(c1);
                m.Comments.Add(c2);
                
                m.Users.Add(u1);
                m.Users.Add(u2);
                m.SaveChanges();

                int currentUser = 1;

                while (true)
                {
                    Console.WriteLine("Commands:\n 1. write comment\n 2. edit comment\n 3. list\n 4. delete comment\n 5. top 5\n 6. Ilgiausias\n 7. Eventu stats\n 8. exit");
                    switch (int.Parse(Console.ReadLine().Substring(0, 1)))
                    {
                        case 1: createComment2(m, currentUser); break;
                        case 2: updateComment2(m, currentUser); break;
                        case 3: list(m); break;
                        case 4: deleteComment2(m, currentUser); break;
                        case 5: top5(m); break;
                        case 6: ilgiausias(m); break;
                        case 7: eventInfo(m); break;
                        case 8: goto loopBreak;
                    }
                    Console.WriteLine("---");
                    Console.WriteLine();
                }
            loopBreak:
                return;
            }
        }

        static void top5(Model1Container m)
        {
            var top = (from user in m.Users
                      orderby user.Comments.Count
                      select user.Name).Take(5);
            int i = 1;
            foreach (var u in top) Console.WriteLine(i++ +". " +u);
        }

        static void eventInfo(Model1Container m)
        {
            foreach (var e in m.Events)
            {
                Console.WriteLine(e.Title + " - " + e.Users.Count + " useriu ir " + e.Comments.Count + " komentaru");
            }
        }

        static void ilgiausias(Model1Container m)
        {
            var ilgiausias = (from comment in m.Comments
                              orderby comment.Text.Length
                              select comment).First().User.Name;
            Console.WriteLine("Ilgiausio komentaro autorius: " + ilgiausias);
        }

        static void createComment(Model1Container m, int user)
        {
            Console.WriteLine("EventTitle Text");
            string[] words = Console.ReadLine().Split(' ');
            string title = words[0];
            Comment comment = new Comment();
            User author = m.Users.Where(u => u.Id.Equals(user)).First();
            Event evnt = m.Events.Where(e => e.Title.Equals(title)).First();
            comment.Text = words.Skip(1).Aggregate((w1, w2) => w1 + " " + w2);
            comment.User = author;
            comment.Event = evnt;
            m.Comments.Add(comment);
            m.SaveChanges();
        }

        static void createComment2(Model1Container m, int user)
        {
            Console.WriteLine("EventTitle Text");
            string[] words = Console.ReadLine().Split(' ');
            string title = words[0];
            string text = words.Skip(1).Aggregate((w1, w2) => w1 + " " + w2);
            SqlConnection con = (SqlConnection)m.Database.Connection;
            con.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO Comments(eventId, userId, text) VALUES ((SELECT Id FROM Events WHERE Title = '"+title+"'), "+user+", '"+text+"');", con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        static void updateComment(Model1Container m, int user)
        {
            var comments = from c in m.Comments
                           where c.UserId == user
                           select c;
            
            foreach (var comment in comments)
            {
                Console.WriteLine(String.Format("{0}. \"{1}\" in {2}", comment.Id, comment.Text, comment.Event.Title));
            }
            Console.WriteLine("id new_text");
            string[] words = Console.ReadLine().Split(' ');
            int targetId = int.Parse(words[0]);
            var comment_edit = (from c in comments
                               where c.Id == targetId
                               select c).First();
            comment_edit.Text = words.Skip(1).Aggregate((w1, w2) => w1 + " " + w2);
            m.SaveChanges();
        }

        static void updateComment2(Model1Container m, int user)
        {
            var comments = from c in m.Comments
                           where c.UserId == user
                           select c;

            foreach (var comment in comments)
            {
                Console.WriteLine(String.Format("{0}. \"{1}\" in {2}", comment.Id, comment.Text, comment.Event.Title));
            }
            Console.WriteLine("id new_text");
            string[] words = Console.ReadLine().Split(' ');
            int targetId = int.Parse(words[0]);
            string text = words.Skip(1).Aggregate((w1, w2) => w1 + " " + w2);
            SqlConnection con = (SqlConnection)m.Database.Connection;
            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Comments SET Text = '"+text+"' WHERE Id = "+targetId+";", con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        static void list(Model1Container m)
        {
            Console.WriteLine("users [name]");
            Console.WriteLine("events [title]");
            Console.WriteLine("comments");
            string input = Console.ReadLine();
            string[] words = input.Split(' ');
            if (words.Length == 1)
            {
                if (words[0].ToLower().Equals("users"))
                {
                    SqlConnection connection = (SqlConnection)m.Database.Connection;
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter adapter = new SqlDataAdapter("SELECT Name FROM Users", connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds, "Users");
                    adapter.Dispose();
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Console.WriteLine("   " + ds.Tables[0].Rows[i]["Name"]);
                    }
                }
                if (words[0].ToLower().Equals("events"))
                {
                    foreach (var e in m.Events) Console.WriteLine("   " + e.Title);
                    return;
                }
                if (words[0].ToLower().Equals("comments"))
                {
                    var commentQuery = from comment in m.Comments
                                       join user in m.Users on comment.UserId equals user.Id
                                       join e in m.Events on comment.EventId equals e.Id
                                       select new { Comment = comment.Text, User = user.Name, Evnt = e.Title};
                    foreach (var c in commentQuery) Console.WriteLine(String.Format("{0} apie {1}: \"{2}\"", c.User, c.Evnt, c.Comment));
                }
            } else if (words.Length >= 2)
            {
                if (words[0].ToLower().Equals("users"))
                {
                    string userName = words[1];
                    var users = m.Users.Select(user => user).Where(user => user.Name.ToLower().Equals(userName));
                    if (users.Count() < 1)
                    {
                        Console.WriteLine("NOT FOUND!");
                        return;
                    }

                    foreach (var user in users)
                    {
                        Console.WriteLine("Name: " + user.Name);
                        Console.WriteLine("Attends: ");
                        foreach (var e in user.Events)  Console.WriteLine(e.Title);
                    }
                }
                if (words[0].ToLower().Equals("events"))
                {
                    string eventTitle = words[1];
                    var events = m.Events.Select(e => e).Where(e => e.Title.ToLower().Equals(eventTitle));
                    if (events.Count() < 1)
                    {
                        Console.WriteLine("NOT FOUND!");
                        return;
                    }

                    foreach (var e in events)
                    {
                        Console.WriteLine("Title: " + e.Title);
                        Console.WriteLine("Attendees: ");
                        foreach (var user in e.Users) Console.WriteLine(user.Name);
                    }
                }
            }
        }

        static void deleteComment(Model1Container m, int user)
        {
            var comments = from c in m.Comments
                           where c.UserId == user
                           select c;

            foreach (var comment in comments)
            {
                Console.WriteLine(String.Format("{0}. \"{1}\" in {2}", comment.Id, comment.Text, comment.Event.Title));
            }
            Console.WriteLine("id to delete");;
            int targetId = int.Parse(Console.ReadLine());
            var comment_delete = (from c in comments
                                where c.Id == targetId
                                select c).First();
            m.Comments.Remove(comment_delete);
            m.SaveChanges();
        }

        static void deleteComment2(Model1Container m, int user)
        {
            var comments = from c in m.Comments
                           where c.UserId == user
                           select c;

            foreach (var comment in comments)
            {
                Console.WriteLine(String.Format("{0}. \"{1}\" in {2}", comment.Id, comment.Text, comment.Event.Title));
            }
            Console.WriteLine("id to delete"); ;
            int targetId = int.Parse(Console.ReadLine());
            SqlConnection con = (SqlConnection)m.Database.Connection;
            con.Open();
            SqlCommand cmd = new SqlCommand("DELETE FROM Comments WHERE Id = "+targetId+";", con);
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }

}
